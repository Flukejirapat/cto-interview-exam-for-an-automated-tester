from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
import time

xpathDropdownIcon = '//*[@id="category-page-content"]/div[2]/div[2]'
xpathDropdownText = '//*[@id="sel-form--"]/div[1]'
xpathMenuCanon = '//*[@id="category-page-content"]/div[2]/div[1]/div/div[2]/div/div[2]/div/div[1]/div/div[2]/div'

PATH = "C:\Program Files (x86)\chromedriver.exe"
driver = webdriver.Chrome(PATH)
driver.get("https://www.officemate.co.th")
clickMenuButton = driver.find_element_by_id("btn-MobileMenu").click()
clickMenuOffice = driver.find_element_by_id("lnk-categoryChild-107").click()

waitMenuCalculator = WebDriverWait(driver, 10).until(
    EC.presence_of_element_located((By.ID, "lnk-categoryChild-892"))
)
clickMenuCalculator = driver.find_element_by_id("lnk-categoryChild-892").click()
clickMenuCalculator = driver.find_element_by_id("lnk-categoryChild-892").click()

waitMenuOrderby = WebDriverWait(driver, 10).until(
    EC.presence_of_element_located((By.XPATH, xpathDropdownIcon))
)
clickDropdown = driver.find_element_by_xpath(xpathDropdownText).click()
clickPriceAsc = driver.find_element_by_xpath('//*[@id="price_asc"]/div').click()

driver.maximize_window()
clickMenuCanon = driver.find_element_by_xpath(xpathMenuCanon).click()

waitMenuOrderby = WebDriverWait(driver, 10).until(
    EC.element_to_be_clickable((By.ID, "btn-addCart-OFM8015945"))
)
clickText = driver.find_element_by_id("txt-AddToCartQty-OFM8015945").click()
clearText = driver.find_element_by_id("txt-AddToCartQty-OFM8015945").send_keys(Keys.DELETE)
inputText = driver.find_element_by_id("txt-AddToCartQty-OFM8015945").send_keys("2")
clickLowestPriceItem = driver.find_element_by_id("btn-addCart-OFM8015945").click()

time.sleep(5)
driver.get("https://www.officemate.co.th/th/cart")

waitMenuOrderby = WebDriverWait(driver, 10).until(
    EC.element_to_be_clickable((By.ID, "txt-AddToCartQty-OFM8015945"))
)
getText = driver.find_element_by_id("lbl-minicartQty").text
assert getText == "2"