def FindDuplicatedNumber(InputArray) :
    Answer = []
    for x in range (0, len(InputArray)) :
        index = abs(InputArray[x])-1
        if InputArray[index] < 0 :
            DuplicatedNumber = index+1
            if DuplicatedNumber not in Answer :
                Answer.append(DuplicatedNumber)
        if InputArray[index] > 0 :
            InputArray[index] = -InputArray[index]
    print("Duplicated :", Answer)
    return Answer
